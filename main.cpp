#include <iostream>
#include <math.h>

class Vector
{
private:
    int x;
    int y;
    int z;
    int d;
public: 
    int GetX()
    {
        return x;
    }

    int GetY()
    {
        return y;
    }

    int GetZ()
    {
        return z;
    }

    void SetX(int newX)
    {
        x = newX;
    }

    void SetY(int newY)
    {
        y = newY;
    }

    void SetZ(int newZ)
    {
        z = newZ;
    }

    int GetD()
    {
        return d;
    }

    void SetD()
    {
        d = (x * x) + (y * y) + (z * z);
        sqrt(d);
    }
};

int main()
{
    Vector tempX, tempY, tempZ, tempD;
    tempX.SetX(10);
    tempY.SetY(2);
    tempZ.SetZ(5);
    tempD.SetD();
    std::cout << tempX.GetX() << ' ' << tempY.GetY() << ' ' << tempZ.GetZ() << ' '<< tempD.GetD();
}